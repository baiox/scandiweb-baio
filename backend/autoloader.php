<?php

function autoLoader($class)
{
    // List of all Directories (Folders) Containing Classes
    $classesDirectories = [
        'core/',
        'core/database',
        'app/controllers/',
        'app/models/',
    ];
    // Looping through directories and adding a require statement for each directory
    foreach ($classesDirectories as $directory) {
        $file = sprintf('%s/%s.php', $directory, $class);
        if (is_file($file)) {
            require_once $file;
        }
    }
}

spl_autoload_register('autoLoader');