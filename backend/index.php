<?php
// Autoload all the project classes
require 'autoloader.php';
// Adding the config file to the Dependency Injector (App)
// the config file has all the database information.
App::bind('config', require 'config.php');
// Getting the database information
$database = App::get('config')['database'];
// Instantiating a PDO object
$pdo = Connection::make($database);
// Passing the PDO object to the Query Builder class
QueryBuilder::connect($pdo);
// Loading the router and passing the routes file
$router = Router::load('routes.php');
// Directing the user based on the current uri and method
$router->direct(Request::uri(), Request::method());
