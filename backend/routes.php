<?php
// API End Points
// 1- GET request to get all products
$router->get('api/products', ['ProductController', 'index']);
// 2- GET request to get all types
$router->get('api/types', ['TypeController', 'index']);
// 3- POST request to add a new product
$router->post('api/products', ['ProductController', 'store']);
// 4- POST request to mass delete the products
$router->post('api/products/delete', ['ProductController', 'destroy']);
