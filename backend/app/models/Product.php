<?php

class Product extends MainProduct
{
    /**
     * @var array
     */
    private array $attributes;
    /**
     * @var array
     */
    private array $values;


    /**
     * @param string $sku
     * @param string $name
     * @param float $price
     * @param int $type_id
     */
    public function __construct(string $sku, string $name, float $price, int $type_id)
    {
        parent::__construct(
            $sku,
            $name,
            $price,
            $type_id,
            Type::getType($type_id)->getName()
        );
    }

    /**
     * returning all the attributes for this Product
     * as TypeAttribute objects and mapping them into arrays
     * @return array
     */
    public function getAttributes(): array
    {
        return array_map(static function ($attribute) {
            return [
                "id" => $attribute->getId(),
                "name" => $attribute->getName(),
                "unit" => $attribute->getUnit()
            ];
        }, $this->attributes);
    }

    /**
     * turning products_values database records into attribute => value pairs
     * @return array
     */
    public function getValues(): array
    {
        $values = [];
        foreach ($this->values as $value) {
            $values[] = [$value['name'] => $value['value']];
        }

        return $values;
    }

    /**
     * @param array $attributes
     */
    public function setAttributes(array $attributes): void
    {
        $this->attributes = $attributes;
    }

    /**
     * @param array $values
     */
    public function setValues(array $values): void
    {
        $this->values = $values;
    }


    /**
     * inserting the new Product into the database
     * @param $product
     * @return void
     */
    public function save($product): void
    {
        $mainProduct = [
            'type_id' => htmlspecialchars($this->getTypeId()),
            'sku' => htmlspecialchars($this->getSku()),
            'name' => htmlspecialchars($this->getName()),
            'price' => htmlspecialchars($this->getPrice()),
        ];
        QueryBuilder::insert('products', $mainProduct);
        $this->setId(QueryBuilder::pdo()->lastInsertId());
        $this->setProductValues($product);
    }

    /**
     * deleting the current Product object
     * from the database.
     * @return void
     */
    public function delete(): void
    {
        QueryBuilder::delete('products', $this->id);
    }

    /**
     * retrieving all the product special attributes values
     * @return array|false|void
     */
    public function getProductValues()
    {
        $sql = sprintf(
            "SELECT a.name, pv.value FROM `products_values` pv
            JOIN `types_attributes` ta ON pv.type_attribute_id = ta.id
            JOIN `attributes` a ON ta.attribute_id = a.id
            WHERE product_id = %s",
            $this->getId()
        );
        return QueryBuilder::query($sql);
    }

    /**
     * inserting the product special attributes values into the database.
     * @param $product
     * @return void
     */
    public function setProductValues($product): void
    {
        foreach ($this->getAttributes() as $attribute) {
            $typeProduct = [
                'product_id' => $this->getId(),
                'type_attribute_id' => $attribute['id'],
                'value' => $product->{$attribute['name']}
            ];
            QueryBuilder::insert('products_values', $typeProduct);
        }
    }
}
