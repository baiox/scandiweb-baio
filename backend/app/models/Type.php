<?php

class Type
{
    /**
     * @var int
     */
    protected int $id;
    /**
     * @var string
     */
    protected string $name;
    /**
     * @var string
     */
    protected string $description;
    /**
     * @var array
     */
    protected array $attributes;

    /**
     * @param int $id
     * @param string $name
     * @param string $description
     * @param array $attributes
     */
    public function __construct(int $id, string $name, string $description, array $attributes)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->attributes = $attributes;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return array_map(static function ($attribute) {
            return [
                "id" => $attribute->getId(),
                "name" => $attribute->getName(),
                "unit" => $attribute->getUnit()
            ];
        }, $this->attributes);
    }

    /**
     * returns an array with objects of class Type
     * @return array
     */
    public static function all(): array
    {
        // getting all the types records from the database
        $typesRecords = QueryBuilder::all('types');
        // mapping on the records and returning an object of Type foreach record
        return array_map(static function ($record) {
            // passing the record columns to the class constructor
            // and getting all the related attributes using the TypeAttribute class
            return new self(
                $record['id'],
                $record['name'],
                $record['description'],
                TypeAttribute::all($record['id'])
            );
        }, $typesRecords);
    }

    /**
     * @param $type_id
     * @return Type
     */
    public static function getType($type_id): Type
    {
        // querying the database for the type record with the given id
        $type = QueryBuilder::get('types', ['id' => $type_id])[0];
        // returning a new instance of class Type
        // and getting all the related attributes using the TypeAttribute class
        return new self(
            $type['id'],
            $type['name'],
            $type['description'],
            TypeAttribute::all($type['id'])
        );
    }


}
