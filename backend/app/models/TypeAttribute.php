<?php

class TypeAttribute
{
    /**
     * @var int|mixed
     */
    protected int $id;
    /**
     * @var string|mixed
     */
    protected string $name;
    /**
     * @var string|mixed
     */
    protected string $unit;

    /**
     * @param array $attribute
     */
    public function __construct(array $attribute)
    {
        $this->id = $attribute['id'];
        $this->name = $attribute['name'];
        $this->unit = $attribute['unit'];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getUnit(): string
    {
        return $this->unit;
    }

    /**
     * getting all the attributes that belong to a specific type
     * with the given type id
     * @param int $type_id
     * @return array
     */
    public static function all(int $type_id): array
    {
        $sql = sprintf(
            "SELECT a.*
                    FROM attributes a
                    INNER JOIN types_attributes ta ON a.id = ta.attribute_id
                    INNER JOIN types t ON ta.type_id = t.id
                    WHERE ta.type_id = %s",
            $type_id
        );
        $attributes = QueryBuilder::query($sql);
        return array_map(static function ($attribute) {
            return new TypeAttribute($attribute);
        }, $attributes);
    }

    /**
     * returns all the attributes no matter what type it belongs to
     * will be useful for adding new types
     * Note: Not used yet
     * @return array
     */
    public static function allAttributes(): array
    {
        $attributes = QueryBuilder::all('attributes', ['name']);
        return array_map(static function ($attribute) {
            return new TypeAttribute($attribute);
        }, $attributes);
    }


}