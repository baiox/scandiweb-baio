<?php

abstract class MainProduct
{
    /**
     * @var int
     */
    protected int $id;
    /**
     * @var string
     */
    protected string $sku;
    /**
     * @var string
     */
    protected string $name;
    /**
     * @var float
     */
    protected float $price;
    /**
     * @var int
     */
    protected int $typeId;
    /**
     * @var string
     */
    protected string $type;

    /**
     * @param $product
     * @return void
     */
    abstract public function save($product): void;

    /**
     * @return void
     */
    abstract public function delete(): void;


    /**
     * @param string $sku
     * @param string $name
     * @param float $price
     * @param int $typeId
     * @param string $type
     */
    public function __construct(string $sku, string $name, float $price, int $typeId, string $type)
    {
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->typeId = $typeId;
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }


    /**
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getTypeId(): int
    {
        return $this->typeId;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * querying all the products from the database
     * and mapping the records to a new Product class
     * @return Product[]
     */
    public static function all(): array
    {
        $productsRecords = QueryBuilder::all('products');
        return array_map(static function ($record) {
            $product = new Product(
                $record['sku'],
                $record['name'],
                (float)$record['price'],
                $record['type_id'],
            );
            $product->setId($record['id']);
            $product->setAttributes(TypeAttribute::all($product->getTypeId()));
            $product->setValues($product->getProductValues());
            return $product;
        }, $productsRecords);
    }

    /**
     * a function to return a Product based on a given id
     * returns false if there is no products with the given id
     * @param $id
     * @return bool|Product
     */
    public static function getProduct($id): bool|Product
    {
        $products = self::all();
        foreach ($products as $product) {
            if ($product->getId() === $id) {
                return $product;
            }
        }
        return false;
    }


}
