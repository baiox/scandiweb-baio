<?php

class ProductController
{

    /**
     * returns all the products as json
     * @return void
     * @throws JsonException
     */
    public function index(): void
    {
        // HTTP headers
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        // getting all the products as objects form the Product class
        $products = Product::all();
        // a check to ensure there is at least one product in the database
        if (count($products) > 0) {
            // mapping all the Product objects into associative array
            $products = array_map(static function ($product) {
                return [
                    "id" => $product->getId(),
                    "sku" => $product->getSku(),
                    "name" => $product->getName(),
                    "price" => $product->getPrice(),
                    "type" => $product->getType(),
                    "attributes" => $product->getAttributes(),
                    "values" => $product->getValues(),
                ];
            }, $products);
            // setting the http response code to 200
            http_response_code(200);
            // echoing the results in JSON
            echo json_encode($products, JSON_THROW_ON_ERROR);
        } else {
            // if there is no products in the database
            // returning response code 404 "Not Found Code"
            http_response_code(200);
            echo json_encode("No products found, try adding some", JSON_THROW_ON_ERROR);
        }
    }


    /**
     * storing a new product
     * @return void
     * @throws JsonException
     */
    public function store(): void
    {
        // HTTP headers
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        // decoding the json data into variable data
        $data = json_decode(file_get_contents("php://input"), false, 512, JSON_THROW_ON_ERROR);
        // retrieving the product object from the data object
        $product = $data->product;
        // validating the product main attributes
        $validator = new Validate();
        $validator->validateType($product->type);
        $validator->validateSku($product->sku);
        $validator->validateName($product->name);
        $validator->validateInt('price', $product->price);
        // validating the special attributes
        foreach (TypeAttribute::all((int)$product->type) as $attribute) {
            $validator->validateInt($attribute->getName(), $product->{$attribute->getName()});
        }
        // a check to handle if there is any errors
        if ($validator->errors()) {
            // if there is errors returning code 400 "Bad Request"
            http_response_code(400);
            // echoing the errors array as a json object to access it from frontend
            echo json_encode($validator->errors(), JSON_THROW_ON_ERROR);
        } else {
            // instantiating a new Product object and passing the values from the json object
            $newProduct = new Product(
                $product->sku,
                $product->name,
                (float)$product->price,
                $product->type
            );
            // setting the special attributes bases on the type id
            $newProduct->setAttributes(TypeAttribute::all($newProduct->getTypeId()));
            // calling the save method on the Product object
            $newProduct->save($product);
            // returning response code 201 "Resource Created"
            http_response_code(201);
            echo json_encode("product created successfully", JSON_THROW_ON_ERROR);
        }
    }


    /**
     * deleting a product or more
     * @return void
     * @throws JsonException
     */
    public function destroy(): void
    {
        // HTTP headers
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        // getting the deleted products ids
        $deletedIds = json_decode(file_get_contents("php://input"), false, 512, JSON_THROW_ON_ERROR);
        // looping through the ids
        foreach ($deletedIds as $id) {
            // calling getProduct method which returns the Product object with the given id
            // and returns false if there is no product with the given id
            $product = Product::getProduct((int)$id);
            if ($product) {
                // calling the delete method on the Product object
                $product->delete();
                // returning response code 200
                http_response_code(200);
                echo json_encode("Product with id $id deleted successfully", JSON_THROW_ON_ERROR);
            } else {
                // returning response code 400 "Bad Request" if there is no product with the given id
                http_response_code(400);
                echo json_encode("There is no products with id $id", JSON_THROW_ON_ERROR);
            }
        }
    }
}
