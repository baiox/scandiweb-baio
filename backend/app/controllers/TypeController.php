<?php

class TypeController
{
    /**
     * returns all the types as json
     * @return void
     * @throws JsonException
     */
    public function index(): void
    {
        // HTTP headers
        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json; charset=UTF-8");
        // getting all the types as objects from the Type class
        $types = Type::all();
        // mapping the objects of class Type into an associative array to return it as JSON
        $types = array_map(static function ($type) {
            return [
                'id' => $type->getId(),
                'name' => $type->getName(),
                'description' => $type->getDescription(),
                'attributes' => $type->getAttributes(),
            ];
        }, $types);
        // setting the http response code to 200 and echoing the results as JSON
        http_response_code(200);
        echo json_encode($types, JSON_THROW_ON_ERROR);
    }
}
// HTTP headers
// header("Access-Control-Allow-Origin: *");
// header("Access-Control-Allow-Credentials: true ");
// header("Content-Type: application/json; charset=UTF-8");
// header("Access-Control-Allow-Methods: OPTIONS, GET, POST");
// header("Access-Control-Allow-Headers: Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control");
