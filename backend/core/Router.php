<?php

// Router class is responsible for directing the user to the right controller

class Router
{
    /**
     * routes array stores the app routes based on method
     * @var array|array[]
     */
    protected array $routes = [
        'GET' => [],
        'POST' => []
    ];

    /**
     * instancing a new Router with a given routes file
     * @param $file
     * @return Router
     */
    public static function load($file): Router
    {
        $router = new self;

        require $file;

        return $router;
    }

    /**
     * pushes the get routes to the routes array
     * @param $uri
     * @param $controller
     * @return void
     */
    public function get($uri, $controller): void
    {
        $this->routes['GET'][$uri] = $controller;
    }

    /**
     * pushes the post routes to the routes array
     * @param $uri
     * @param $controller
     * @return void
     */
    public function post($uri, $controller): void
    {
        $this->routes['POST'][$uri] = $controller;
    }

    /**
     * directing the user based on the uri and the method by calling the controller method
     * if there is a route defined in the routes array
     * @param $uri
     * @param $requestType
     * @return mixed|void
     */
    public function direct($uri, $requestType)
    {
        if (array_key_exists($uri, $this->routes[$requestType])) {
            return $this->callController(...$this->routes[$requestType][$uri]);
        }

        echo 'sorry no resources here';
        die();
    }

    /**
     * calls the action on the controller given in the routes file
     * @param $controller
     * @param $action
     * @return mixed
     */
    private function callController($controller, $action): mixed
    {
        return (new $controller)->$action();
    }
}
