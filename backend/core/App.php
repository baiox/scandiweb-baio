<?php

// Dependency Injector class

class App
{
    /**
     * registry array is responsible for saving all the dependencies
     * @var array
     */
    protected static array $registry = [];

    /**
     * bind function pushes values into the registry array
     * @param $key
     * @param $value
     * @return void
     */
    public static function bind($key, $value): void
    {
        static::$registry[$key] = $value;
    }

    /**
     * get function gets values from the registry array
     * @param $key
     * @return mixed
     */
    public static function get($key): mixed
    {
        return static::$registry[$key];
    }
}
