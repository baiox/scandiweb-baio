<?php

// Query Builder is the class responsible for querying the database

abstract class QueryBuilder
{
    /**
     * @var PDO
     */
    protected static PDO $pdo;

    /**
     * connect function assigns the pdo object to the Query Builder class
     * @param $pdo
     * @return void
     */
    public static function connect($pdo): void
    {
        self::$pdo = $pdo;
    }

    /**
     * @return PDO
     */
    public static function pdo(): PDO
    {
        return self::$pdo;
    }

    /**
     * generic function to query the database directly
     * @param $sql
     * @return array|false|void
     */
    public static function query($sql)
    {
        $conn = self::$pdo;
        try {
            return $conn->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    /**
     * gets all the records from a given table
     * @param string $table
     * @param array $columns
     * @return array|false|void
     */
    public static function all(string $table, array $columns = ['*'])
    {
        $conn = self::$pdo;
        // changing the columns from "name" to "table.name"
        $tableColumns = self::columnsToSQL($table, $columns);

        $sql = sprintf(
            "SELECT %s
                        FROM %s 
                        GROUP BY %s",
            implode(', ', $tableColumns),
            $table,
            $table . '.id'
        );

        try {
            return $conn->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    /**
     * inserts parameters into given table using prepared statements to protect from SQL injection
     * @param string $table
     * @param array $parameters
     * @return void
     */
    public static function insert(string $table, array $parameters): void
    {
        $conn = self::$pdo;
        $sql = sprintf(
            "INSERT INTO %s (%s) VALUES (%s)",
            $table,
            implode(', ', array_keys($parameters)),
            ':' . implode(', :', array_keys($parameters))
        );
        try {
            $statement = $conn->prepare($sql);
            $statement->execute($parameters);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    /**
     * gets a specific record(s) from a table
     * @param string $table
     * @param array $where
     * @param array $columns
     * @return array|false|void
     */
    public static function get(string $table, array $where, array $columns = ['*'])
    {
        $conn = self::$pdo;
        $tableColumns = self::columnsToSQL($table, $columns);
        $sql = sprintf(
            "SELECT %s
                    FROM %s 
                    WHERE %s = '%s'",
            implode(', ', $tableColumns),
            $table,
            $table . '.' . array_keys($where)[0],
            array_values($where)[0]
        );
        try {
            return $conn->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    /**
     * deletes a record or more from a given table based on a given id or ids
     * @param string $table
     * @param int $id
     * @return void
     */
    public static function delete(string $table, int $id): void
    {
        $conn = self::$pdo;
        $sql = sprintf(
            'DELETE FROM %s WHERE %s.id IN (%s)',
            $table,
            $table,
            $id
        );
        try {
            $conn->query($sql);
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    /**
     * function to add the table name to the given columns
     * @param string $table
     * @param array $columns
     * @return array
     */
    private static function columnsToSQL(string $table, array $columns): array
    {
        $output = [];
        foreach ($columns as $column) {
            $output[] = $table . '.' . $column;
        }
        return $output;
    }
}
