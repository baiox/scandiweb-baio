<?php

// Connection class to instantiate PDO object

class Connection
{
    /**
     * @param $databaseInfo
     * @return PDO|void
     */
    public static function make($databaseInfo)
    {
        try {
            return new PDO(
                $databaseInfo['connection'] . ';dbname=' . $databaseInfo['name'],
                $databaseInfo['username'],
                $databaseInfo['password'],
                $databaseInfo['options']
            );
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}
