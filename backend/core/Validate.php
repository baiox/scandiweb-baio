<?php

class Validate
{
    /**
     * @var array
     */
    private array $errors = [];

    /**
     * check to ensure sku is not empty and 14 characters long and unique
     * @param string $sku
     * @return void
     */
    public function validateSku(string $sku): void
    {
        if (empty($sku)) {
            $this->errors["sku"] = "SKU can't be blank";
        } elseif (strlen($sku) < 10) {
            $this->errors["sku"] = "SKU must be at least 10 charcters";
        } elseif (!$this->skuUnique($sku)) {
            $this->errors["sku"] = "SKU must be Unique";
        }
    }

    /**
     * check to ensure type_id is not empty and refers to an actual type
     * @param $type_id
     * @return void
     */
    public function validateType($type_id): void
    {
        if (empty($type_id)) {
            $this->errors["type"] = "You have to choose a type";
        } elseif (!$this->typeExists($type_id)) {
            $this->errors["type"] = "Type id doesn't refer to an actual type";
        }
    }

    /**
     * check to ensure name is not empty and has more than 2 characters and not a number
     * @param string $name
     * @return void
     */
    public function validateName(string $name): void
    {
        if (empty($name)) {
            $this->errors["name"] = "Name can't be blank";
        } elseif (ctype_digit($name)) {
            $this->errors["name"] = "Name can't be numbers";
        } elseif (strlen($name) < 2) {
            $this->errors["name"] = "Name can't be less than 2 characters";
        }
    }

    /**
     * check to ensure integer input is not empty and a number
     * @param $input
     * @param $value
     * @return void
     */
    public function validateInt($input, $value): void
    {
        if (empty($value)) {
            $this->errors[$input] = $input . " can't be blank";
        } elseif (ctype_alpha($value)) {
            $this->errors[$input] = $input . " must be numbers";
        }
    }

    /**
     * @param string $sku
     * @return bool
     */
    private function skuUnique(string $sku): bool
    {
        // Executing a query to get the products with the same SKU
        $sql = sprintf("select sku from products where sku = '%s'", $sku);
        $productsWithSku = QueryBuilder::query($sql);
        // a check to see if there is any products with the same sku returning false if there is
        return count($productsWithSku) <= 0;
    }

    /**
     * @param int $type_id
     * @return bool
     */
    private function typeExists(int $type_id): bool
    {
        // Executing a query to get the type with the provided id
        $sql = sprintf("select * from types where id = '%s'", $type_id);
        $type = QueryBuilder::query($sql);
        // a check to see if there is any types with the provided id returning true if there is
        return count($type) > 0;
    }

    /**
     * returns the errors array if it's not empty
     * if it's returns false
     * @return bool|array
     */
    public function errors(): bool|array
    {
        if (empty($this->errors)) {
            return false;
        }
        return $this->errors;
    }
}
