module.exports = {
  devServer: {
    proxy: {
      "^/api/": {
        target: "https://scandiweb-baio.herokuapp.com",
        changeOrigin: true, // so CORS doesn't bite us.
      },
    },
  },
};
