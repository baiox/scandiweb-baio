import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import AddProduct from "../views/AddProduct.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/add-product",
    name: "AddProduct",
    component: AddProduct,
  },
  // redirect "addproduct" to "add-product"
  {
    path: "/addproduct",
    redirect: "/add-product",
  },
  //catch all and redirct to home
  {
    path: "/:catchAll(.*)",
    redirect: "/",
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
